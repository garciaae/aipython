import matplotlib.pyplot as plt
from mesa.batchrunner import BatchRunner

from wealth_model.model import compute_gini
from wealth_model.model import MoneyModel

fixed_params = {
    "grid_width": 10,
    "grid_height": 10,
}

variable_params = {
    "number_of_agents": range(10, 500, 10),
}

batch_run = BatchRunner(
    MoneyModel,
    fixed_parameters=fixed_params,
    variable_parameters=variable_params,
    iterations=5,
    max_steps=100,
    model_reporters={"Gini": compute_gini}
)

batch_run.run_all()

run_data = batch_run.get_model_vars_dataframe()
run_data.head()
plt.scatter(run_data.number_of_agents, run_data.Gini)
plt.show()
