import random

from mesa import Agent
from mesa import Model
from mesa.datacollection import DataCollector

# Represents the world
from mesa.space import MultiGrid
from mesa.time import RandomActivation


def compute_gini(model):
    agents_wealth = [agent.wealth for agent in model.schedule.agents]
    x = sorted(agents_wealth)
    N = model.num_agents
    B = sum(xi * (N-i) for i, xi in enumerate(x)) / (N*sum(x))
    return 1 + (1/N) - 2*B


class MoneyAgent(Agent):
    """ Represents a single agent"""
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.wealth = 1

    def step(self):
        self.move()
        if self.wealth > 0:
            self.give_money()

    def move(self):
        # Moore is for 8 neighbours
        possible_steps = self.model.grid.get_neighborhood(
            self.pos,
            moore=True,
            include_center=False,
        )
        new_position = random.choice(possible_steps)
        self.model.grid.move_agent(self, new_position)

    def give_money(self):
        cellmates = self.model.grid.get_cell_list_contents([self.pos])
        if len(cellmates) > 1:
            other = random.choice(cellmates)
            other.wealth += 1
            self.wealth -= 1


class MoneyModel(Model):
    """ It contains the general model"""
    def __init__(self, number_of_agents, grid_width, grid_height):
        self.running = True
        self.num_agents = number_of_agents
        self.grid = MultiGrid(grid_width, grid_height, True)
        self.schedule = RandomActivation(self)

        # Create agents
        for i in range(self.num_agents):
            agent = MoneyAgent(i, self)
            self.schedule.add(agent)
            x = random.randrange(self.grid.width)
            y = random.randrange(self.grid.height)
            self.grid.place_agent(agent, (x,y))

        self.datacollector = DataCollector(
            model_reporters={"Gini": compute_gini},
            agent_reporters={"Wealth": lambda a: a.wealth},
        )

    def step(self):
        self.datacollector.collect(self)
        self.schedule.step()
