import random
import numpy as np

from mesa import Model
from mesa.datacollection import DataCollector
from mesa.space import MultiGrid

from simulated_model.agents import Climber
from simulated_model.agents import Patch
from simulated_model.agents import MountainPatch
from simulated_model.constants import COOLING_RATE
from simulated_model.constants import HILL_HEIGHT
from simulated_model.constants import MAX_ELEVATION
from simulated_model.constants import MIN_TEMPERATURE
from simulated_model.constants import NUMBER_OF_HILLS
from simulated_model.schedule import RandomActivationByBreed


class GridModel(Model):
    def __init__(self, number_of_agents, width, height, patch_model=Patch, mapped=False):
        self.N = number_of_agents    # num of agents
        self.schedule = RandomActivationByBreed(self)
        self.grid = MultiGrid(width, height, torus=False)
        self.mapped = mapped
        self.elevation_matrix = self.get_zero_matrix()

        if mapped:
            # add a patch per cell
            [[self.grid.place_agent(patch_model(self), (x, y)) for x in range(width)] for y in range(height)]
        self.running = True

    def print(self):
        for x in range(self.grid.width):
            result = [self.grid.get_cell_list_contents((x, y))[0].elevation for y in range(self.grid.height)]
            print(result)

    def agent_at(self, pos, layer=0):
        if not pos or layer < 0:
            raise ValueError
        return self.grid.get_cell_list_contents(pos)[layer]

    def get_zero_matrix(self):
        return np.zeros((self.grid.height, self.grid.width))

    def get_elevation_matrix(self):
        result = []
        for x in range(self.grid.width):
            result.append([self.grid.get_cell_list_contents((x, y))[0].elevation for y in range(self.grid.height)])
        return np.array(result)

    def get_latest_elevation_matrix(self):
        return self.elevation_matrix

    def set_elevation_matrix(self, matrix):
        """
        Accumulate a heights matrix for the model
        :param matrix: Map representing the heights for each tile
        """
        for x in range(self.grid.width):
            for y in range(self.grid.height):
                self.grid.get_cell_list_contents((x, y))[0].elevation += matrix[x, y]
        self.elevation_matrix = matrix

    def _diffuse(self, pos, number=1, matrix=None):
        """
        give equal shares of (number * 100) percent of the value of
        patch-variable to its eight neighboring patches.
        If a patch has fewer than eight neighbors, each neighbor
        still gets an eighth share
        :param pos:(int, int)  position of the patch
        :param number: (float) percentage to be shared
        :return result: [(int, int, ...), ..., (int, int, ...)] matrix with the results for a single cell
        """
        result = self.get_zero_matrix()
        neighborhood_positions = self.grid.get_neighborhood(pos, moore=True)
        number_of_neighbors = len(neighborhood_positions)
        cell_value = matrix[pos[0], pos[1]]
        spread_rate = number / 8
        spread_value = cell_value * spread_rate
        leftover_rate = number - (number_of_neighbors * spread_rate)
        leftover_value = cell_value * leftover_rate
        # Assignments
        result[pos[0], pos[1]] = leftover_value
        for neighbor_position in neighborhood_positions:
            result[neighbor_position[0], neighbor_position[1]] += spread_value
        return result

    def _get_boundaries(self, pos, radius):
        # assert pos[0] - radius >= 0 and pos[0] + radius < self.grid.width
        # assert pos[1] - radius >= 0 and pos[1] + radius < self.grid.height
        return (pos[0] - radius, pos[1] - radius), (pos[0] + radius, pos[1] + radius)

    def diffuse(self, pos, number, repeat):
        if not self.mapped:
            return
        assert 0 <= number <= 1
        intermediate = None
        results = self.get_zero_matrix()
        matrix = self._diffuse(pos, number, self.get_elevation_matrix())
        for radius in range(repeat):
            boundaries = self._get_boundaries(pos, radius + 1)
            intermediate = self.get_zero_matrix()
            for x in range(boundaries[0][0], min(boundaries[1][0] + 1, self.grid.height)):
                for y in range(boundaries[0][1], min(boundaries[1][1] + 1, self.grid.height)):
                    intermediate += self._diffuse((x, y), number, matrix)
            matrix = np.copy(intermediate)
            results += intermediate
        return intermediate if repeat > 0 else matrix

    def step(self):
        self.schedule.step()


class ClimbModelSimulatedAnnealing(GridModel):
    def __init__(
            self,
            number_of_agents,
            width,
            height,
            max_elevation,
            temperature,
    ):
        super().__init__(
            number_of_agents=number_of_agents,
            width=width,
            height=height,
            patch_model=MountainPatch,
            mapped=True,
        )
        self.max_elevation = max_elevation
        self.width = width
        self.height = height
        self.temperature = temperature
        self.generate_landscape(number_of_hills=NUMBER_OF_HILLS, height=HILL_HEIGHT)
        self.make_walker_agents()
        self.running = True
        self.datacollector = DataCollector(
            {
                "Temperature": lambda m: m.temperature,
                "Energy": lambda m: m.global_energy
            }
        )

    def set_fixed_landscape(self):
        # (TODO): Manually create a fixed landscape
        elevation_matrix = [
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00244140625, 0.01318359375,
             0.044677734375, 0.10302734375, 0.18310546875, 0.25341796875, 0.283447265625, 0.25341796875, 0.18310546875,
             0.10302734375, 0.044677734375, 0.01318359375, 0.00244140625, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.006103515625, 0.0260009765625,
             0.0823974609375, 0.1727294921875, 0.296630859375, 0.3936767578125, 0.4410400390625, 0.3936767578125,
             0.296630859375, 0.1727294921875, 0.0823974609375, 0.0260009765625, 0.006103515625, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.010986328125, 0.0439453125,
             0.13916015625, 0.28564453125, 0.4925537109375, 0.648193359375, 0.7305908203125, 0.648193359375,
             0.4925537109375, 0.28564453125, 0.13916015625, 0.0439453125, 0.010986328125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.015380859375, 0.059326171875,
             0.1875, 0.377197265625, 0.648193359375, 0.84228515625, 0.9521484375, 0.84228515625, 0.648193359375,
             0.377197265625, 0.1875, 0.059326171875, 0.015380859375, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.017333984375, 0.066650390625,
             0.2120361328125, 0.428466796875, 0.7415771484375, 0.967529296875, 33.0791015625, 0.967529296875,
             0.7415771484375, 0.428466796875, 0.2120361328125, 0.066650390625, 0.017333984375, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0001220703125, 0.000732421875, 0.0025634765625,
             0.006103515625, 0.010986328125, 0.031494140625, 0.0802001953125, 0.214599609375, 0.413818359375,
             0.6982421875, 0.9041748046875, 1.018798828125, 0.9017333984375, 0.692138671875, 0.40283203125, 0.19921875,
             0.06298828125, 0.01611328125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.000732421875, 0.003662109375, 0.01171875,
             0.025634765625, 0.0439453125, 0.0728759765625, 0.12158203125, 0.23583984375, 0.41015625, 0.6573486328125,
             0.847412109375, 0.9437255859375, 0.83642578125, 0.6317138671875, 0.3662109375, 0.176513671875,
             0.0556640625, 0.0135498046875, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0025634765625, 0.01171875, 0.037353515625,
             0.08056640625, 0.13916015625, 0.19970703125, 0.2607421875, 0.3486328125, 0.4736328125, 0.65185546875,
             0.791748046875, 0.8564453125, 0.7569580078125, 0.5712890625, 0.33447265625, 0.1611328125, 0.05126953125,
             0.01220703125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.006103515625, 0.025634765625, 0.08056640625,
             0.167236328125, 0.28564453125, 0.3907470703125, 0.47802734375, 0.5537109375, 0.65185546875,
             0.7989501953125, 0.916259765625, 0.9656982421875, 0.841796875, 0.6317138671875, 0.3662109375,
             0.176513671875, 0.0556640625, 0.0135498046875, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.010986328125, 0.0439453125, 0.13916015625,
             0.28564453125, 0.4925537109375, 0.664306640625, 0.7935791015625, 0.847412109375, 0.8953857421875,
             0.977783203125, 1.040771484375, 1.06201171875, 0.91259765625, 0.692138671875, 0.40283203125, 0.19921875,
             0.06298828125, 0.01611328125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0001220703125, 0.01611328125, 0.0618896484375,
             0.193603515625, 0.38818359375, 0.66357421875, 0.8768310546875, 1.0341796875, 1.0653076171875,
             1.082763671875, 1.121337890625, 1.15576171875, 33.0963134765625, 0.98291015625, 0.7415771484375,
             0.428466796875, 0.2120361328125, 0.066650390625, 0.017333984375, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.000732421875, 0.0208740234375, 0.07763671875,
             0.235107421875, 0.46630859375, 0.7899169921875, 1.033447265625, 33.138427734375, 1.18359375,
             1.1334228515625, 1.082275390625, 1.055419921875, 1.018798828125, 0.8594970703125, 0.648193359375,
             0.377197265625, 0.1875, 0.059326171875, 0.015380859375, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0025634765625, 0.027099609375, 0.0966796875, 0.26806640625,
             0.516357421875, 0.835693359375, 1.062744140625, 1.18359375, 1.12060546875, 1.014404296875, 0.9071044921875,
             0.847412109375, 0.79248046875, 0.66357421875, 0.4925537109375, 0.28564453125, 0.13916015625, 0.0439453125,
             0.010986328125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0001220703125, 0.000732421875, 0.0086669921875, 0.042724609375,
             0.135498046875, 0.32177734375, 0.5885009765625, 0.8851318359375, 1.087646484375, 1.1395263671875,
             1.0169677734375, 0.8277587890625, 0.6519775390625, 0.5419921875, 0.472412109375, 0.38818359375,
             0.28564453125, 0.167236328125, 0.08056640625, 0.025634765625, 0.006103515625, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.000732421875, 0.003662109375, 0.022705078125, 0.07568359375,
             0.208740234375, 0.425537109375, 0.7257080078125, 0.9931640625, 1.154296875, 1.10791015625, 0.9188232421875,
             0.655517578125, 0.4462890625, 0.31201171875, 0.24609375, 0.193603515625, 0.13916015625, 0.08056640625,
             0.037353515625, 0.01171875, 0.0025634765625, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0025634765625, 0.01171875, 0.052734375, 0.1424560546875,
             0.33837890625, 0.60205078125, 0.938232421875, 1.1689453125, 1.279541015625, 1.135986328125, 0.884765625,
             0.5537109375, 0.3145751953125, 0.156005859375, 0.093017578125, 0.0618896484375, 0.0439453125,
             0.025634765625, 0.01171875, 0.003662109375, 0.000732421875, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.006103515625, 0.025634765625, 0.0977783203125, 0.23388671875,
             0.498779296875, 0.811279296875, 1.1785888671875, 1.373291015625, 33.4241943359375, 1.18603515625,
             0.873046875, 0.498046875, 0.252197265625, 0.093017578125, 0.0380859375, 0.01611328125, 0.010986328125,
             0.006103515625, 0.0025634765625, 0.000732421875, 0.0001220703125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.010986328125, 0.0439453125, 0.154541015625, 0.3450927734375,
             0.6807861328125, 1.0279541015625, 1.3848876953125, 1.50146484375, 1.4600830078125, 1.1451416015625,
             0.802734375, 0.43212890625, 0.20458984375, 0.0618896484375, 0.01611328125, 0.0001220703125, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.015380859375, 0.059326171875, 0.198486328125, 0.421142578125,
             0.787353515625, 1.1279296875, 1.4447021484375, 1.490478515625, 1.3787841796875, 1.025390625,
             0.6800537109375, 0.344970703125, 0.154541015625, 0.0439453125, 0.010986328125, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0172119140625, 0.06591796875, 0.215576171875, 0.447998046875,
             0.8111572265625, 1.119384765625, 33.0791015625, 1.329345703125, 1.1529541015625, 0.799560546875,
             0.4951171875, 0.233154296875, 0.0977783203125, 0.025634765625, 0.006103515625, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.015380859375, 0.059326171875, 0.1900634765625, 0.388916015625,
             0.685546875, 0.9228515625, 1.09130859375, 1.02978515625, 0.857666015625, 0.564697265625, 0.32666015625,
             0.139892578125, 0.052734375, 0.01171875, 0.0025634765625, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.010986328125, 0.0439453125, 0.139892578125, 0.289306640625,
             0.5042724609375, 0.673828125, 0.7745361328125, 0.70751953125, 0.5584716796875, 0.344970703125, 0.18310546875,
             0.069580078125, 0.022705078125, 0.003662109375, 0.000732421875, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0],
            [0.00537109375, 0.0108642578125, 0.015380859375, 0.0172119140625, 0.015380859375, 0.010986328125,
             0.006103515625, 0.0086669921875, 0.0263671875, 0.080810546875, 0.16796875, 0.2882080078125, 0.38330078125,
             0.433349609375, 0.392578125, 0.3028564453125, 0.1826171875, 0.091552734375, 0.03173828125, 0.0086669921875,
             0.000732421875, 0.0001220703125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.029296875, 0.04541015625, 0.0596923828125, 0.06591796875, 0.059326171875, 0.0439453125, 0.025634765625,
             0.0142822265625, 0.015380859375, 0.0380859375, 0.08056640625, 0.13916015625, 0.1875, 0.20947265625, 0.1875,
             0.13916015625, 0.08056640625, 0.037353515625, 0.01171875, 0.0025634765625, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.10107421875, 0.146484375, 0.1893310546875, 0.20947265625, 0.1875, 0.13916015625, 0.08056640625, 0.0380859375,
             0.015380859375, 0.0142822265625, 0.025634765625, 0.0439453125, 0.059326171875, 0.06591796875, 0.059326171875,
             0.0439453125, 0.025634765625, 0.01171875, 0.003662109375, 0.000732421875, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.236083984375, 0.30810546875, 0.3826904296875, 0.42236328125, 0.377197265625, 0.28564453125, 0.167236328125,
             0.0806884765625, 0.0263671875, 0.0086669921875, 0.006103515625, 0.010986328125, 0.015380859375,
             0.0172119140625, 0.015380859375, 0.010986328125, 0.006103515625, 0.0025634765625, 0.000732421875,
             0.0001220703125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.424072265625, 0.5364990234375, 0.6591796875, 0.7305908203125, 0.648193359375, 0.4925537109375, 0.28564453125,
             0.13916015625, 0.0439453125, 0.010986328125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.59033203125, 0.714111328125, 0.8587646484375, 0.9521484375, 0.84228515625, 0.648193359375, 0.377197265625,
             0.1875, 0.059326171875, 0.015380859375, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.661865234375, 0.8045654296875, 0.9708251953125, 33.0791015625, 0.9521484375, 0.7305908203125, 0.42236328125,
             0.20947265625, 0.06591796875, 0.0172119140625, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.59033203125, 0.714111328125, 0.8587646484375, 0.9521484375, 0.84228515625, 0.648193359375, 0.377197265625,
             0.1875, 0.059326171875, 0.015380859375, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.424072265625, 0.5364990234375, 0.6591796875, 0.7305908203125, 0.648193359375, 0.4925537109375, 0.28564453125,
             0.13916015625, 0.0439453125, 0.010986328125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.236083984375, 0.30810546875, 0.3826904296875, 0.42236328125, 0.377197265625, 0.28564453125, 0.167236328125,
             0.08056640625, 0.025634765625, 0.006103515625, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.10107421875, 0.146484375, 0.1893310546875, 0.20947265625, 0.1875, 0.13916015625, 0.08056640625,
             0.037353515625, 0.01171875, 0.0025634765625, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.029296875, 0.04541015625, 0.0596923828125, 0.06591796875, 0.059326171875, 0.0439453125, 0.025634765625,
             0.01171875, 0.003662109375, 0.000732421875, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            [0.00537109375, 0.0108642578125, 0.015380859375, 0.0172119140625, 0.015380859375, 0.010986328125,
             0.006103515625, 0.0025634765625, 0.000732421875, 0.0001220703125, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        ]
        self.set_elevation_matrix(elevation_matrix)

    def generate_landscape(self, number_of_hills, height):
        """ Create a landscape with hills and valleys
        :param number_of_hills: Number of hills
        :param height: Height of each mountain
        """
        for i in range(number_of_hills):
            top_position = (
                random.randrange(self.width),
                random.randrange(self.height)
            )
            # Get the tile in a random place and set the elevation
            tile = self.grid.get_cell_list_contents(top_position)[0]
            tile.elevation = self.max_elevation
            # Accrue this mountain in the model after diffusing it
            self.set_elevation_matrix(self.diffuse(top_position, 1, height))

    def make_walker_agents(self):
        """ Create some climbers in the ground """
        for i in range(self.N):
            elevation = 1.0
            while elevation != 0.0:
                x = random.randrange(self.width)
                y = random.randrange(self.height)
                elevation = self.agent_at((x, y)).elevation
            climber = Climber((x, y), model=self)
            self.grid.place_agent(climber, (x, y))
            self.schedule.add(climber)

    @property
    def global_energy(self):
        """ Amount of energy for the system

        Amount of energy defined as the difference between the patch under
        the agent and the max elevation for a hill.
        :rtype: integer
        """
        return self.energy_for_solution(self.agents())

    def get_global_energy(self):
        return self.global_energy

    @staticmethod
    def energy_for_solution(array):
        """ Amount of energy for a given array of agents

        Amount of energy defined as the difference between the patch under
        the agent and the max elevation for a hill.
        """
        energy = sum([MAX_ELEVATION - a.elevation for a in array])
        return energy

    def energy_for_positions(self, array):
        """ Amount of energy for a given array of positions

        Amount of energy defined as the difference between the patch under
        the agent and the max elevation for a hill.
        """
        matrix = self.get_elevation_matrix()
        energy = sum([MAX_ELEVATION - matrix[a[0]][a[1]] for a in array])
        return energy

    def agents(self, model=Climber):
        return self.schedule.agents_by_breed[model]

    def agents_but(self, agent):
        result = []
        for a in self.agents():
            if agent.pos is not a.pos:
                result.extend([a,])
        return result

    def agents_positions(self, array=None):
        if array is None:
            array = self.agents()
        return [x.pos for x in array]

    def run_model(self):
        """ Run the model until the end condition is reached. Overload as
        needed.

        """
        while all([a.on_hill for a in self.schedule.agents_by_breed[Climber]]):
            self.schedule.step()

    def step(self):
        # We decrease the global temperature in every single step
        self.temperature = self.temperature - (1 - COOLING_RATE / 100)
        finished = self.temperature < MIN_TEMPERATURE or self.global_energy < 0
        print("Temperature: {0:.3g} Global Energy: {1:.3g}".format(
            self.temperature, self.global_energy)
        )
        if finished:
            print("Finished")
            self.running = False
        else:
            self.schedule.step()
            self.datacollector.collect(self)
