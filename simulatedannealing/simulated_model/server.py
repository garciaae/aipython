from mesa.visualization.modules import CanvasGrid
from mesa.visualization.modules import ChartModule
from mesa.visualization.ModularVisualization import ModularServer

from simulated_model.constants import color_dic
from simulated_model.constants import HEIGHT
from simulated_model.constants import INITIAL_TEMPERATURE
from simulated_model.constants import MAX_ELEVATION
from simulated_model.constants import NUMBER_OF_AGENTS
from simulated_model.constants import PIXEL_RATIO
from simulated_model.constants import WIDTH
from simulated_model.model import Climber
from simulated_model.model import MountainPatch
from simulated_model.model import ClimbModelSimulatedAnnealing


def hillclimbing_portrayal(agent):
    if agent is None:
        return

    portrayal = {}
    if type(agent) is Climber:
        portrayal = {
            "Shape": "circle",
            "Filled": "true",
            "r": 0.5,
            "Layer": 1,
            "Color": "red",
        }
    elif type(agent) is MountainPatch:
        if agent.elevation > 0:
            color_index = min(int(agent.elevation * 16), MAX_ELEVATION)
            portrayal["Color"] = color_dic[round(color_index)]
        else:
            portrayal["Color"] = "#35682d"
        portrayal["Shape"] = "rect"
        portrayal["Filled"] = "true"
        portrayal["Layer"] = 0
        portrayal["w"] = 1
        portrayal["h"] = 1

    return portrayal


canvas_element = CanvasGrid(hillclimbing_portrayal, 20, 20, 500, 500)
chart_element = ChartModule(
    [
        {
            "Label": "Temperature", "Color": "#AA0000",
            "Label": "Energy", "Color": "Green",
        },
    ],
    canvas_width=WIDTH * PIXEL_RATIO,
)


def launch_climb_model_simulated_annealing():
    width = WIDTH
    height = HEIGHT
    num_agents = NUMBER_OF_AGENTS
    pixel_ratio = PIXEL_RATIO

    grid = CanvasGrid(
        hillclimbing_portrayal,
        width,
        height,
        width * pixel_ratio,
        height * pixel_ratio,
    )
    server = ModularServer(
        ClimbModelSimulatedAnnealing,
        [grid, chart_element],
        "Climb the hill",
        {
            "number_of_agents": num_agents,
            "width": width,
            "height": height,
            "max_elevation": MAX_ELEVATION,
            "temperature": INITIAL_TEMPERATURE,
        },
    )
    server.launch()
