color_dic = {
    0: '#000000',
    1: '#080808',
    2: '#101010',
    3: '#181818',
    4: '#202020',
    5: '#282828',
    6: '#303030',
    7: '#383838',
    8: '#404040',
    9: '#484848',
    10: '#505050',
    11: '#585858',
    12: '#606060',
    13: '#686868',
    14: '#707070',
    15: '#787878',
    16: '#808080',
    17: '#888888',
    18: '#909090',
    19: '#989898',
    20: '#A0A0A0',
    21: '#A8A8A8',
    22: '#B0B0B0',
    23: '#B8B8B8',
    24: '#C0C0C0',
    25: '#C8C8C8',
    26: '#D0D0D0',
    27: '#D8D8D8',
    28: '#E0E0E0',
    29: '#E8E8E8',
    30: '#F0F0F0 ',
    31: '#F8F8F8',
    32: '#FFFFFF',
}

MAX_ELEVATION = 32
HEIGHT = 35
WIDTH = 35
PIXEL_RATIO = 18
NUMBER_OF_HILLS = 10
HILL_HEIGHT = 5
NUMBER_OF_AGENTS = 6

# Simulated annealing
COOLING_RATE = 5  # 5 means 5%
MIN_TEMPERATURE = 2
INITIAL_TEMPERATURE = 100
