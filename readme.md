Python snipets for AI
---------------------

In this repository we want to cover some of the topics covered in AI classes at *Universidad de Sevilla*.

Guidelines to run the proyects
------------------------------
**Jupyter notebook based code.**

We have, at this moment, the following notebooks:
* `graphtool/espacios.ipynb`
* `graphtool/busqueda_informada.ipynb`

In order to properly get them running we should follow the instructions below.

1. Install Docker: [https://docs.docker.com/engine/installation/]
2. Get this ready to go image by running: `docker pull tiagopeixoto/graph-tool`
3. Run the image: `docker run -p 8888:8888 -p 6006:6006 -it -u user -w /home/user tiagopeixoto/graph-tool bash`
4. Now get the token by running the following at the container's prompt: `jupyter notebook --ip 0.0.0.0`
5. Once we get the token we can run both the notebooks above.

**Mesa based projects**

We have the following projects written for the mesa based projects:
* Hill climbing: [https://gitlab.com/garciaae/aipython/tree/master/hillclimbing]
* Simulated Annealing [https://gitlab.com/garciaae/aipython/tree/master/simulatedannealing]

In order to run Mesa library based projects we shoud follow these steps:
1. Install `virtualenv` and `virtualenvwrapper`
2. Create a fresh environment: `mkvirtualenv aipython`
3. Activate the environment, if it hasn't been already activated by running: `workon aipython`
4. Install the requirements: `pip install -r ./requirements.txt`
5. Browse the projects and run: `python ./run.py`
