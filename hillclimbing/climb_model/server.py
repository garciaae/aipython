from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer

from climb_model.constants import color_dic
from climb_model.constants import MAX_ELEVATION
from climb_model.constants import HEIGHT
from climb_model.constants import WIDTH
from climb_model.model import Climber
from climb_model.model import MountainPatch
from climb_model.model import ClimbModel


def hillclimbing_portrayal(agent):
    if agent is None:
        return

    portrayal = {}
    if type(agent) is Climber:
        portrayal = {
            "Shape": "circle",
            "Filled": "true",
            "r": 0.5,
            "Layer": 1,
            "Color": "red",
        }
    elif type(agent) is MountainPatch:
        if agent.elevation > 0:
            color_index = min(int(agent.elevation * 16), MAX_ELEVATION)
            portrayal["Color"] = color_dic[round(color_index)]
        else:
            portrayal["Color"] = "#35682d"
        portrayal["Shape"] = "rect"
        portrayal["Filled"] = "true"
        portrayal["Layer"] = 0
        portrayal["w"] = 1
        portrayal["h"] = 1

    return portrayal


def launch_climb_model():
    width = WIDTH
    height = HEIGHT
    num_agents = 6
    pixel_ratio = 18

    grid = CanvasGrid(
        hillclimbing_portrayal,
        width,
        height,
        width * pixel_ratio,
        height * pixel_ratio,
    )
    server = ModularServer(
        ClimbModel,
        [grid],
        "Climb the hill",
        {
            "number_of_agents": num_agents,
            "width": width,
            "height": height,
            "max_elevation": MAX_ELEVATION,
        },
    )
    #server.max_steps = 0
    #server.port = 8521
    server.launch()
