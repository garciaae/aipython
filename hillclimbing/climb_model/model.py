import random
import numpy as np

from mesa import Model
from mesa.space import MultiGrid

from climb_model.agents import Climber
from climb_model.agents import Patch
from climb_model.agents import MountainPatch
from climb_model.constants import HILL_HEIGHT
from climb_model.constants import NUMBER_OF_HILLS
from climb_model.schedule import RandomActivationByBreed


class GridModel(Model):
    def __init__(self, number_of_agents, width, height, patch_model=Patch, mapped=False):
        self.N = number_of_agents    # num of agents
        self.schedule = RandomActivationByBreed(self)
        self.grid = MultiGrid(width, height, torus=False)
        self.mapped = mapped
        if mapped:
            # add a patch per cell
            [[self.grid.place_agent(patch_model(self), (x, y)) for x in range(width)] for y in range(height)]
        self.running = True

    def print(self):
        for x in range(self.grid.width):
            result = [self.grid.get_cell_list_contents((x, y))[0].elevation for y in range(self.grid.height)]
            print(result)

    def agent_at(self, pos, layer=0):
        if not pos or layer < 0:
            raise ValueError
        return self.grid.get_cell_list_contents(pos)[layer]

    def get_zero_matrix(self):
        return np.zeros((self.grid.height, self.grid.width))

    def get_elevation_matrix(self):
        result = []
        for x in range(self.grid.width):
            result.append([self.grid.get_cell_list_contents((x, y))[0].elevation for y in range(self.grid.height)])
        return np.array(result)

    def set_elevation_matrix(self, matrix):
        """
        Accumulate a heights matrix for the model
        :param matrix: Map representing the heights for each tile
        """
        for x in range(self.grid.width):
            for y in range(self.grid.height):
                self.grid.get_cell_list_contents((x, y))[0].elevation += matrix[x, y]

    def _diffuse(self, pos, number=1, matrix=None):
        """
        give equal shares of (number * 100) percent of the value of
        patch-variable to its eight neighboring patches.
        If a patch has fewer than eight neighbors, each neighbor
        still gets an eighth share
        :param pos:(int, int)  position of the patch
        :param number: (float) percentage to be shared
        :return result: [(int, int, ...), ..., (int, int, ...)] matrix with the results for a single cell
        """
        result = self.get_zero_matrix()
        neighborhood_positions = self.grid.get_neighborhood(pos, moore=True)
        number_of_neighbors = len(neighborhood_positions)
        cell_value = matrix[pos[0], pos[1]]
        spread_rate = number / 8
        spread_value = cell_value * spread_rate
        leftover_rate = number - (number_of_neighbors * spread_rate)
        leftover_value = cell_value * leftover_rate
        # Assignments
        result[pos[0], pos[1]] = leftover_value
        for neighbor_position in neighborhood_positions:
            result[neighbor_position[0], neighbor_position[1]] += spread_value
        return result

    def _get_boundaries(self, pos, radius):
        # assert pos[0] - radius >= 0 and pos[0] + radius < self.grid.width
        # assert pos[1] - radius >= 0 and pos[1] + radius < self.grid.height
        return (pos[0] - radius, pos[1] - radius), (pos[0] + radius, pos[1] + radius)

    def diffuse(self, pos, number, repeat):
        if not self.mapped:
            return
        assert 0 <= number <= 1
        intermediate = None
        results = self.get_zero_matrix()
        matrix = self._diffuse(pos, number, self.get_elevation_matrix())
        for radius in range(repeat):
            boundaries = self._get_boundaries(pos, radius + 1)
            intermediate = self.get_zero_matrix()
            for x in range(boundaries[0][0], min(boundaries[1][0] + 1, self.grid.height)):
                for y in range(boundaries[0][1], min(boundaries[1][1] + 1, self.grid.height)):
                    intermediate += self._diffuse((x, y), number, matrix)
            matrix = np.copy(intermediate)
            results += intermediate
        return intermediate if repeat > 0 else matrix

    def step(self):
        print("Model step")
        self.schedule.step()


class ClimbModel(GridModel):
    def __init__(self, number_of_agents, width, height, max_elevation):
        super().__init__(
            number_of_agents=number_of_agents,
            width=width,
            height=height,
            patch_model=MountainPatch,
            mapped=True,
        )
        self.max_elevation = max_elevation
        self.width = width
        self.height = height
        self.generate_landscape(number_of_hills=NUMBER_OF_HILLS, height=HILL_HEIGHT)
        self.make_walker_agents()
        self.running = True

    def generate_landscape(self, number_of_hills, height):
        """ Create a landscape with hills and valleys
        :param number_of_hills: Number of hills
        :param height: Height of each mountain
        """
        for i in range(number_of_hills):
            top_position = (
                random.randrange(self.width),
                random.randrange(self.height)
            )
            # Get the tile in a random place and set the elevation
            tile = self.grid.get_cell_list_contents(top_position)[0]
            tile.elevation = self.max_elevation
            # Accrue this mountain in the model after diffusing it
            self.set_elevation_matrix(self.diffuse(top_position, 1, height))

    def make_walker_agents(self):
        """ Create some climbers in the ground """
        for i in range(self.N):
            elevation = 1.0
            while elevation != 0.0:
                x = random.randrange(self.width)
                y = random.randrange(self.height)
                elevation = self.agent_at((x, y)).elevation
            climber = Climber((x, y), model=self)
            self.grid.place_agent(climber, (x, y))
            self.schedule.add(climber)

    def run_model(self):
        """ Run the model until the end condition is reached. Overload as
        needed.

        """
        while all([a.on_hill for a in self.schedule.agents_by_breed[Climber]]):
            self.schedule.step()

    def step(self):
        finished = all([a.on_hill for a in self.schedule.agents_by_breed[Climber]])
        if finished:
            print("Finished")
            self.running = False
        else:
            self.schedule.step()
