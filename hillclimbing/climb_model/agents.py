import random

from mesa import Agent


class Turtle(Agent):
    def __init__(self, model, pcolor=None):
        super().__init__(model, pcolor)
        self.pcolor = pcolor

    def _get_instance(self, pos, model):
        """ Return the instance of model at pos or None"""
        result = None
        for item in self.model.grid.get_cell_list_contents(pos):
            if isinstance(item, model):
                result = item
                break
        return result


class Patch(Agent):
    def __init__(self, model, pcolor=None):
        super().__init__(model, pcolor)
        self.pcolor = pcolor

    def step(self):
        print("patch step")


class MountainPatch(Patch):
    def __init__(self, model, elevation=0, color=None):
        super().__init__(model, pcolor=color)
        self.elevation = elevation


class Climber(Turtle):

    def __init__(self, pos, model, moore=True):
        assert(model is not None)
        super().__init__(pos, model)
        self.pos = pos
        self.moore = moore
        self.in_the_top = False

    @property
    def on_hill(self):
        """ Did we reach the top?"""
        return self.in_the_top

    def uphill(self):
        """Moves the agent to the neighboring patch with the highest value"""
        if self.in_the_top:
            return
        next_moves = self.model.grid.get_neighborhood(self.pos, self.moore, True)
        next_move = self.pos
        max_elevation = self._get_instance(self.pos, MountainPatch).elevation
        for move in next_moves:
            # MultiGrids have a list of unordered objects per cell. Getting the patch.
            elevation = self._get_instance(move, MountainPatch).elevation
            if elevation > max_elevation:
                max_elevation = elevation
                next_move = move

        # If we are in the floor, move wherever
        if max_elevation == 0.0:
            self.random_move()
            return

        # We didn't move, we are in a local maximum
        if next_move == self.pos:
            self.in_the_top = True
            print("Reached the top")
            return
        # Up the hill
        self.model.grid.move_agent(self, next_move)

    def random_move(self):
        next_moves = self.model.grid.get_neighborhood(self.pos, self.moore, True)
        next_move = random.choice(next_moves)
        self.model.grid.move_agent(self, next_move)

    def step(self):
        self.uphill()
